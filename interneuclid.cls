% Time-stamp: <2015-04-14 22:08:01 ycopin>
\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{interneuclid}[2015/04/09 Euclid Internal Note Class]
% All options are passed to article class
\LoadClassWithOptions{article}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}

%% usefull packages
\RequirePackage{fancyhdr}
\RequirePackage[usenames,dvipsnames]{color}
\RequirePackage{ifthen}
\RequirePackage{graphicx}
\RequirePackage{vmargin}
\RequirePackage{multirow}
\RequirePackage{cellspace}
\RequirePackage{colortbl}
\RequirePackage{lastpage}
\RequirePackage{enumitem}
\RequirePackage{url}
\RequirePackage{longtable}

\pagestyle{fancy}

%%%%%%%%%%%%%%%%%%%%

%% Define margins {right}{top}{left}{bottom}
\setmarginsrb{2truecm}{1.2truecm}{2truecm}{1.5truecm}
{1.5truecm}{3truecm}{1.5truecm}{1truecm}

\hbadness=10000
\emergencystretch=\hsize
\tolerance=9999

\renewcommand{\topfraction}{0.9}    % max fraction of floats at top
\renewcommand{\bottomfraction}{0.1} % max fraction of floats at bottom

\renewcommand{\labelitemi}{$-$}
\setlist{nolistsep}

%%%%%%%%%%%%%%%%%%%%

%% names
\newcommand{\@reference}{}
\newcommand{\reference}[1]{\renewcommand{\@reference}{#1}}

\newcommand{\@issue}{}
\newcommand{\issue}[1]{\renewcommand{\@issue}{#1}}

\newcommand{\@custodian}{}
\newcommand{\custodian}[1]{\renewcommand{\@custodian}{#1}}

\newcommand{\@summary}{}
\newcommand{\summary}[1]{\renewcommand{\@summary}{#1}}

\newcommand{\@writtendate}{}
\newcommand{\writtendate}[1]{\renewcommand{\@writtendate}{#1}}
\newcommand{\@contributiondate}{}
\newcommand{\contributiondate}[1]{\renewcommand{\@contributiondate}{#1}}
\newcommand{\@approveddate}{}
\newcommand{\approveddate}[1]{\renewcommand{\@approveddate}{#1}}
\newcommand{\@authorizeddate}{}
\newcommand{\authorizeddate}[1]{\renewcommand{\@authorizeddate}{#1}}

\newcommand{\@authors}{}
\newcommand{\authors}[1]{\renewcommand{\@authors}{
    \begin{minipage}{\linewidth}
            #1
    \end{minipage}
}}

\newcommand{\@contributors}{}
\newcommand{\contributors}[1]{\renewcommand{\@contributors}{
    \begin{minipage}{\linewidth}
            #1
    \end{minipage}
}}

\newcommand{\@approved}{}
\newcommand{\approved}[1]{\renewcommand{\@approved}{
    \begin{minipage}{\linewidth}
            #1
    \end{minipage}
}}

\newcommand{\@authorized}{}
\newcommand{\authorized}[1]{\renewcommand{\@authorized}{
    \begin{minipage}{\linewidth}
            #1
    \end{minipage}
}}

\newcommand{\add}[2]{#1 \emph{(#2)} \\}

\newcommand{\@modifications}{}
\newcommand{\modifications}[1]{\renewcommand{\@modifications}{
    \begin{tabular}{|p{1.8cm}|p{1.9cm}|p{2.1cm}|p{6cm}|p{4cm}|}
      \hline
      \rowcolor[gray]{0.85}
      \begin{minipage}{\linewidth}
        \center
        \textbf{Issue}
      \end{minipage} &
      \begin{minipage}{\linewidth}
        \center
        \textbf{Date}
      \end{minipage} &
      \begin{minipage}{\linewidth}
        \center
        \textbf{Page}
      \end{minipage} &
      \begin{minipage}{\linewidth}
        \center
        \textbf{Description of changes}
      \end{minipage} &
      \begin{minipage}{\linewidth}
        \center
        \textbf{Comments}
      \end{minipage} \\
      \hline
      #1
    \end{tabular}
}}

\newcommand{\addmod}[5]{
\begin{minipage}{\linewidth}
    \center
    #1
\end{minipage} &
\begin{minipage}{\linewidth}
    \center
    #2
\end{minipage} &
\begin{minipage}{\linewidth}
    \center
    #3
\end{minipage} &
\begin{minipage}{\linewidth}
    #4
\end{minipage} &
\begin{minipage}{\linewidth}
    #5
\end{minipage} \\ \hline }

\newcommand{\@purpose}{}
\newcommand{\purpose}[1]{\renewcommand{\@purpose}{#1}}

\newcommand{\@scope}{}
\newcommand{\scope}[1]{\renewcommand{\@scope}{#1}}

\newcommand{\@applicabledoc}{No applicable document.}
\newcommand{\applicabledoc}[1]{\renewcommand{\@applicabledoc}{
    \begin{longtable}{|p{0.8cm}|p{9.5cm}|p{3.8cm}|p{1.8cm}|}
      \hline
      \rowcolor[gray]{0.85}
      \begin{minipage}{\linewidth}
        \center
        \textbf{RD}
      \end{minipage} &  &
      \textbf{Ref.}  &
      \textbf{Date} \\
      \hline
      \endfirsthead

      \hline
      \rowcolor[gray]{0.85}
      \begin{minipage}{\linewidth}
        \center
        \textbf{AD}
      \end{minipage} &  &
      \textbf{Ref.}  &
      \textbf{Date} \\
      \hline
      \endhead

      \multicolumn{4}{|r|}{{Continued on next page}} \\ \hline
      \endfoot

      \endlastfoot
      #1
    \end{longtable}
}}

\newcommand{\@referencedoc}{No reference document.}
\newcommand{\referencedoc}[1]{\renewcommand{\@referencedoc}{
    \begin{longtable}{|p{0.8cm}|p{9.5cm}|p{3.8cm}|p{1.8cm}|}
      \hline
      \rowcolor[gray]{0.85}
      \begin{minipage}{\linewidth}
        \center
        \textbf{RD}
      \end{minipage} &  &
      \textbf{Ref.}  &
      \textbf{Date} \\
      \hline
      \endfirsthead

      \hline
      \rowcolor[gray]{0.85}
      \begin{minipage}{\linewidth}
        \center
        \textbf{RD}
      \end{minipage} &  &
      \textbf{Ref.}  &
      \textbf{Date} \\
      \hline
      \endhead

      \multicolumn{4}{|r|}{{Continued on next page}} \\ \hline
      \endfoot

      \endlastfoot
      #1
    \end{longtable}
}}

\newcommand{\adddoc}[4]{
\begin{minipage}{\linewidth}
    \center
    #1
\end{minipage} &
\begin{minipage}{\linewidth}
    #2
\end{minipage} &
\begin{minipage}{\linewidth}
    % \url{#3} % Let the user choose
    #3
\end{minipage} &
\begin{minipage}{\linewidth}
    #4
\end{minipage} \\ \hline }

\newcommand{\@acronyms}{}
\newcommand{\acronyms}[1]{\renewcommand{\@acronyms}{
    \begin{longtable}{p{1.5cm}p{13.4cm}}
      #1
    \end{longtable}
}}

\newcommand{\addacro}[2]{
\begin{minipage}{\linewidth}
    #1
\end{minipage} &
\begin{minipage}{\linewidth}
    #2
\end{minipage} \\}

%%%%%%%%%%%%%%%%%%%%

% Proper fix for having continuous page numbering in the title page
%
% From http://tex.stackexchange.com/questions/171976/forcing-page-number-to-start-from-the-title-page
%
\renewenvironment{titlepage}
 {%
  \if@twocolumn
    \@restonecoltrue\onecolumn
  \else
    \@restonecolfalse\newpage
  \fi
  \thispagestyle{fancy}%
 }
 {%
  \if@restonecol
    \twocolumn
  \else
    \newpage
  \fi
 }

%% redefine the \maketitle command
\renewcommand{\maketitle}{
  \begin{titlepage}
    \thispagestyle{fancy}
    \begin{center}
      \begin{tabular}{|p{4cm}|p{3.4cm}|p{2.9cm}|p{5.6cm}|}
        \hline
        & \multicolumn{3}{l|}{} \\
        \begin{minipage}{\linewidth}
            \raggedleft
            \large \textbf{Title:}
        \end{minipage} &
        \multicolumn{3}{l|}{
        \begin{minipage}{10.8cm}
            \large \@title
        \end{minipage}} \\
        & \multicolumn{3}{l|}{} \\
        \hline
        & & & \\
        \begin{minipage}{\linewidth}
            \raggedleft
            \large \textbf{Date:}
        \end{minipage} & \large \@date &
        \begin{minipage}{\linewidth}
            \raggedleft
            \large \textbf{Issue:}
        \end{minipage} & \large \@issue \\
        & & & \\
        \hline
        & \multicolumn{3}{l|}{} \\
            \raggedleft
        \textbf{\large Reference:} & \multicolumn{3}{l|}{
        \begin{minipage}{10.8cm}
            \large \@reference
        \end{minipage}} \\
        & \multicolumn{3}{l|}{} \\
        \hline
        & \multicolumn{3}{l|}{} \\
        \begin{minipage}{\linewidth}
            \raggedleft
            \large \textbf{Custodian:}
        \end{minipage}  & \multicolumn{3}{l|}{
        \begin{minipage}{10.8cm}
            \large \@custodian
        \end{minipage}} \\
        & \multicolumn{3}{l|}{} \\
        \hline
      \end{tabular}
    \end{center}
    \begin{center}
      \vspace{0.8cm}
    \end{center}
    \begin{center}
      \begin{tabular}{|p{2.9cm}|p{5cm}|p{1.9cm}|p{4cm}|}
        \hline
        \rowcolor[gray]{0.85}
        \begin{minipage}{\linewidth}
            \raggedleft
             \textbf{Authors:}
        \end{minipage} & &
        \textbf{Date:} &
        \textbf{Signature:} \\
        \hline
        & & & \\
        & \@authors & \@writtendate & \\
        & & & \\
        \hline
        \rowcolor[gray]{0.85}
        \begin{minipage}{\linewidth}
            \raggedleft
             \textbf{Contributors:}
        \end{minipage} & & & \\
        \hline
        & & & \\
        & \@contributors & \@contributiondate & \\
        & & & \\
        \hline
        \rowcolor[gray]{0.85}
        \begin{minipage}{\linewidth}
            \raggedleft
             \textbf{Approved by:}
        \end{minipage} & & & \\
        \hline
        & & & \\
        & \@approved & \@approveddate & \\
        & & & \\
        \hline
        \rowcolor[gray]{0.85}
        \begin{minipage}{\linewidth}
            \raggedleft
             \textbf{Authorized by:}
        \end{minipage} & & & \\
        \hline
        & & & \\
        & \@authorized & \@authorizeddate & \\
        & & & \\
        \hline
      \end{tabular}
    \end{center}
  \end{titlepage}

  \clearpage
  \begin{center}
    \large{\textbf{Document version tracking}}
  \end{center}
  \begin{center}
    \@modifications
  \end{center}
  \clearpage
  \begin{center}
    \vspace{0.3cm}
  \end{center}
  \renewcommand\contentsname{\Large \textcolor{CadetBlue}{Table of contents}}
  \tableofcontents
  \clearpage
  \section{Purpose}
    \@purpose
  \section{Scope}
    \@scope
  \section{Applicable \& Reference documents}
  \subsection{Applicable documents}
    \@applicabledoc
  \subsection{Reference documents}
    \@referencedoc
  \section{Acronyms}
    \begin{center}
      \@acronyms
    \end{center}
}
\makeatother

%%%%%%%%%%%%%%%%%%%%

%% define head and foot notes
\fancyhead{}
\fancyfoot{}
\renewcommand{\footrulewidth}{0pt}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\headsep}{30pt}
\fancyhead[L]{}
\fancyhead[R]{}
\fancyfoot[L]{}
\fancyfoot[R]{}

\makeatletter
\chead{
  \begin{tabular}{|p{2cm}|p{9cm}|p{5.5cm}|}
    \hline
    \begin{minipage}{\linewidth}
      \begin{center}
      {\fontsize{36}{46}\selectfont \textbf{EC}}
      \end{center}
    \end{minipage}
    &
    \begin{minipage}{\linewidth}
      \begin{center}
        {\fontsize{18}{28}\selectfont \textbf{\@title}}
      \end{center}
    \end{minipage}
    &
    \begin{minipage}{\linewidth}
    \large
    \begin{tabular}{r|l}
      \textbf{Ref.}:    & \@reference \\
      \textbf{Issue:}   & \@issue     \\
      \textbf{Date:}    & \@date      \\
      \textbf{Page:}    & \thepage/\pageref{LastPage} \\
      \end{tabular}
    \end{minipage}
    \\
    \hline
  \end{tabular} \\
}
\makeatother

\cfoot{
    \begin{minipage}{1.15\linewidth}
      \begin{center}
{\fontsize{7}{8}\selectfont The presented document is Proprietary information of the Euclid Consortium. \\
This document shall be used and disclosed by the receiving Party and its related entities (e.g. contractors and subcontractors) only for the purposes \\
of fulfilling the receiving Party's responsibilities under the Euclid Project and that the identified and marked technical data shall not be disclosed or \\
retransferred to any other entity without prior written permission of the document preparer. \\}
      \end{center}
    \end{minipage}
}

% Bibliography and bibfile
\def\aj{AJ}%
          % Astronomical Journal
\def\actaa{Acta Astron.}%
          % Acta Astronomica
\def\araa{ARA\&A}%
          % Annual Review of Astron and Astrophys
\def\apj{ApJ}%
          % Astrophysical Journal
\def\apjl{ApJ}%
          % Astrophysical Journal, Letters
\def\apjs{ApJS}%
          % Astrophysical Journal, Supplement
\def\ao{Appl.~Opt.}%
          % Applied Optics
\def\apss{Ap\&SS}%
          % Astrophysics and Space Science
\def\aap{A\&A}%
          % Astronomy and Astrophysics
\def\aapr{A\&A~Rev.}%
          % Astronomy and Astrophysics Reviews
\def\aaps{A\&AS}%
          % Astronomy and Astrophysics, Supplement
\def\azh{AZh}%
          % Astronomicheskii Zhurnal
\def\baas{BAAS}%
          % Bulletin of the AAS
\def\bac{Bull. astr. Inst. Czechosl.}%
          % Bulletin of the Astronomical Institutes of Czechoslovakia
\def\caa{Chinese Astron. Astrophys.}%
          % Chinese Astronomy and Astrophysics
\def\cjaa{Chinese J. Astron. Astrophys.}%
          % Chinese Journal of Astronomy and Astrophysics
\def\icarus{Icarus}%
          % Icarus
\def\jcap{J. Cosmology Astropart. Phys.}%
          % Journal of Cosmology and Astroparticle Physics
\def\jrasc{JRASC}%
          % Journal of the RAS of Canada
\def\mnras{MNRAS}%
          % Monthly Notices of the RAS
\def\memras{MmRAS}%
          % Memoirs of the RAS
\def\na{New A}%
          % New Astronomy
\def\nar{New A Rev.}%
          % New Astronomy Review
\def\pasa{PASA}%
          % Publications of the Astron. Soc. of Australia
\def\pra{Phys.~Rev.~A}%
          % Physical Review A: General Physics
\def\prb{Phys.~Rev.~B}%
          % Physical Review B: Solid State
\def\prc{Phys.~Rev.~C}%
          % Physical Review C
\def\prd{Phys.~Rev.~D}%
          % Physical Review D
\def\pre{Phys.~Rev.~E}%
          % Physical Review E
\def\prl{Phys.~Rev.~Lett.}%
          % Physical Review Letters
\def\pasp{PASP}%
          % Publications of the ASP
\def\pasj{PASJ}%
          % Publications of the ASJ
\def\qjras{QJRAS}%
          % Quarterly Journal of the RAS
\def\rmxaa{Rev. Mexicana Astron. Astrofis.}%
          % Revista Mexicana de Astronomia y Astrofisica
\def\skytel{S\&T}%
          % Sky and Telescope
\def\solphys{Sol.~Phys.}%
          % Solar Physics
\def\sovast{Soviet~Ast.}%
          % Soviet Astronomy
\def\ssr{Space~Sci.~Rev.}%
          % Space Science Reviews
\def\zap{ZAp}%
          % Zeitschrift fuer Astrophysik
\def\nat{Nature}%
          % Nature
\def\iaucirc{IAU~Circ.}%
          % IAU Cirulars
\def\aplett{Astrophys.~Lett.}%
          % Astrophysics Letters
\def\apspr{Astrophys.~Space~Phys.~Res.}%
          % Astrophysics Space Physics Research
\def\bain{Bull.~Astron.~Inst.~Netherlands}%
          % Bulletin Astronomical Institute of the Netherlands
\def\fcp{Fund.~Cosmic~Phys.}%
          % Fundamental Cosmic Physics
\def\gca{Geochim.~Cosmochim.~Acta}%
          % Geochimica Cosmochimica Acta
\def\grl{Geophys.~Res.~Lett.}%
          % Geophysics Research Letters
\def\jcp{J.~Chem.~Phys.}%
          % Journal of Chemical Physics
\def\jgr{J.~Geophys.~Res.}%
          % Journal of Geophysics Research
\def\jqsrt{J.~Quant.~Spec.~Radiat.~Transf.}%
          % Journal of Quantitiative Spectroscopy and Radiative Trasfer
\def\memsai{Mem.~Soc.~Astron.~Italiana}%
          % Mem. Societa Astronomica Italiana
\def\nphysa{Nucl.~Phys.~A}%
          % Nuclear Physics A
\def\physrep{Phys.~Rep.}%
          % Physics Reports
\def\physscr{Phys.~Scr}%
          % Physica Scripta
\def\planss{Planet.~Space~Sci.}%
          % Planetary Space Science
\def\procspie{Proc.~SPIE}%
          % Proceedings of the SPIE
\let\astap=\aap
\let\apjlett=\apjl
\let\apjsupp=\apjs
\let\applopt=\ao


